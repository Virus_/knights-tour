import com.sun.tools.javac.Main;

public class KnightTour {
    static int[][] board = new int[8][8];
    static final int ONE = 1, TWO = 2, ZERO = 0, SEVEN = 7;
    static int[] horizontal = {2,1,-1,-2,-2,-1,1,2};
    static int[] vertical = {-1,-2,-2,-1,1,2,2,1};

    static int[][] accessibility = {{2,3,4,4,4,4,3,2},
                                    {3,4,6,6,6,6,4,3},
                                    {4,6,8,8,8,8,6,4},
                                    {4,6,8,8,8,8,6,4},
                                    {4,6,8,8,8,8,6,4,},
                                    {4,6,8,8,8,8,6,4},
                                    {3,4,6,6,6,6,4,3},
                                    {2,3,4,4,4,4,3,2}};

    static  int currentRow = 0 ,currentColumn = 0;
    static int moveNumber;
    static int i = 0;
    static int fullJourney;

    public static void main(String [] args) {
        KnightTour ();
        System.out.println (fullJourney);
    }
    static void KnightTour() { // funckcja odpowiedzialna za logike poruszania sie skoczka po planszy

        while (i < 64) {
            if (canMove ( )) {
                currentRow += vertical[moveNumber];
                currentColumn += horizontal[moveNumber];
                board[currentRow][currentColumn] = ONE;
                fullJourney++;

                // Stosuje petle while w ktorej deklaruje warunek i < 64, poniewaz pelna podroz skoczka po
                // planszy 8 x 8 zajmuje 64 ruchy bo 8 do potegi 2 = 64, currentRow i currentColumn przechowuja obecne
                // wspolrzedne skoczka, vertical i horizontal przechowuja okreslone wspolrzedne, ruchy skoczka w ksztalcie litery L

                printTab ( );
                System.out.println ( );

            } else {
                moveNumber++;
                if (moveNumber > SEVEN) {
                    moveNumber = ZERO;
                }
            }
            i++;}}
    static boolean canMove() { // funckja odpowiedzialna za sprawdzenie czy nastepny ruch nie bedzie wiekszy od
        // dlugosci tablicy oraz czy nie bedzie wiekszy od jej szerokosci, korzystamy z macierzy 8x8
        // wywolujemy rowniez metode ifBusy ktora sprawdza czy nastepne pole na ktore chcemy przeniesc skoczka nie bylo
        // juz zajete wczesniej. Jesli warunek zwroci true to przemieszczamy sie o wskazany wektor.
        if (currentRow + vertical[moveNumber]  < board.length &&
                currentColumn + horizontal[moveNumber] < board[currentColumn].length
                && currentRow + vertical[moveNumber] >= ZERO && currentColumn + horizontal[moveNumber] >= ZERO
                && ifBusy ()){
            return true;
        } else
            return false;
    }
    static void printTab() { // funkcja odpowiedzialna za wydrukowanie macierzy w konsoli.
        for (int i = 0; i < board.length; i++) {
            System.out.println ( );
            for (int j = 0; j < board[i].length; j++) {
                System.out.print (board[i][j]);}}}
   static boolean ifBusy() {
        return  board[currentRow + vertical[moveNumber]][currentColumn + horizontal[moveNumber]] == 0;
    }




}
